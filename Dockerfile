#########################
# Create base container #
#########################
FROM ubuntu:24.04 as base
LABEL maintainer="pidydx"

# Base setup
ENV APP_USER=opensearch
ENV APP_GROUP=opensearch

ENV OPENSEARCH_VERSION=2.16.0

# Create app user and group
RUN userdel ubuntu
RUN groupadd -g 1000 ${APP_GROUP} \
 && useradd -m -N -u 1000 ${APP_USER} -g ${APP_GROUP} -d /usr/local/share/opensearch -s /usr/sbin/nologin

# Update and install base packages
RUN apt-get update -q \
 && DEBIAN_FRONTEND=noninteractive apt-get upgrade -yq \
 && DEBIAN_FRONTEND=noninteractive apt-get install -yq --no-install-recommends ca-certificates \
 && apt-get update -q \
 && DEBIAN_FRONTEND=noninteractive apt-get install -yq --no-install-recommends ${BASE_PKGS} \
 && rm -rf /var/lib/apt/lists/*


##########################
# Create build container #
##########################
FROM base AS builder

# Set build dependencies
ENV BUILD_DEPS wget

# Install build dependencies
RUN apt-get update -q \
 && DEBIAN_FRONTEND=noninteractive apt-get install -yq --no-install-recommends ${BUILD_DEPS}

# Run build
WORKDIR /usr/src
RUN wget -nv https://artifacts.opensearch.org/releases/bundle/opensearch/${OPENSEARCH_VERSION}/opensearch-${OPENSEARCH_VERSION}-linux-x64.tar.gz
RUN mkdir -p /usr/local/share/opensearch
RUN tar -zxf opensearch-${OPENSEARCH_VERSION}-linux-x64.tar.gz -C /usr/local/share/opensearch --strip-components=1
RUN sed -i -e 's/OPENSEARCH_DISTRIBUTION_TYPE=tar/OPENSEARCH_DISTRIBUTION_TYPE=docker/' /usr/local/share/opensearch/bin/opensearch-env
RUN chmod +x /usr/local/share/opensearch/plugins/opensearch-security/tools/*.sh


##########################
# Create final container #
##########################
FROM base

# Prepare container
COPY etc/ /etc/
COPY --from=builder /usr/local /usr/local/
COPY usr/ /usr/

ENV OPENSEARCH_CONTAINER=true
ENV OPENSEARCH_PATH_CONF=/var/lib/opensearch/config
ENV OPENSEARCH_HOME=/usr/local/share/opensearch
ENV PATH=/usr/local/share/opensearch/bin:/usr/local/share/opensearch/plugins/opensearch-security/tools:$PATH
ENV JAVA_HOME=/usr/local/share/opensearch/jdk

RUN chown -R ${APP_USER}:0 /usr/local/share/opensearch \
 && mkdir -p /var/lib/opensearch \
 && chown ${APP_USER}:${APP_GROUP} /var/lib/opensearch 

EXPOSE 9200/tcp
VOLUME ["/etc/opensearch", "/var/lib/opensearch"]

USER $APP_USER
ENTRYPOINT ["docker-entrypoint.sh"]
CMD ["opensearch"]
