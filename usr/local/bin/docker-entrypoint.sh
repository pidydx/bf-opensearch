#!/bin/bash

set -e

if [ "$1" = 'init' ]; then
    mkdir -p /var/lib/opensearch/{config,logs,data}
    echo "Copying configs..."
    cp -r /usr/local/share/opensearch/config/* $OPENSEARCH_PATH_CONF/
    cp -r /etc/opensearch/* $OPENSEARCH_PATH_CONF/
    cp /etc/ssl/certs/ca-certificates.crt $OPENSEARCH_PATH_CONF/
    openssl pkcs8 -topk8 -inform PEM -outform PEM -nocrypt -in $OPENSEARCH_PATH_CONF/admin_rsa_key.pem -out $OPENSEARCH_PATH_CONF/admin_key.pem
    openssl pkcs8 -topk8 -inform PEM -outform PEM -nocrypt -in $OPENSEARCH_PATH_CONF/node_rsa_key.pem -out $OPENSEARCH_PATH_CONF/node_key.pem
    find $OPENSEARCH_PATH_CONF/ -type d -exec chmod 700 {} \;
    find $OPENSEARCH_PATH_CONF/ -type f -exec chmod 600 {} \;
    echo "The following command must be run to initialize the security module and any time the security configurations change."
    exec echo "/usr/local/share/opensearch/plugins/opensearch-security/tools/securityadmin.sh -cd /usr/local/share/opensearch/plugins/opensearch-security/securityconfig/ -icl -nhnv -cacert /etc/ssl/certs/ca-certificates.crt -cert /var/lib/opensearch/config/admin_crt.pem  -key /var/lib/opensearch/config/admin_key.pem"
fi

if [ "$1" = 'opensearch' ]; then
    exec opensearch
fi

exec "$@"